const axios = require('axios');
const credenciais = require('./config');
const rewards = require('./rewards');
// console.log(rewards);
// console.log(credenciais);
let timeRefresh = (60000 * 60) * 3;

const instance2 = axios.create({
    baseURL: 'https://api.streamelements.com/',
    headers: {'Authorization': `Bearer ${credenciais.token_streamElements_jockerz}`}
});

let channels = [
    {
        token_twitch:credenciais.token_twitch_jockerz,
        reward:rewards.reward_jockerz,
        broadcaster_id:credenciais.broadcaster_id_jockerz,
        refresh_token_twitch:credenciais.refresh_token_twitch_jockerz
    }
    ,
    {
        token_twitch:credenciais.token_twitch_duartanath,
        reward:rewards.reward_duartanath,
        broadcaster_id:credenciais.broadcaster_id_duartanath,
        refresh_token_twitch:credenciais.refresh_token_twitch_duartanath
    }
    // ,
    // {
    //     token_twitch:credenciais.token_twitch_teste,
    //     reward:rewards.reward_teste,
    //     broadcaster_id:credenciais.broadcaster_id_teste
    // }
]

// console.log(channels);

async function inicio() {
    for (let i = 0; i < channels.length; i++) {
        for (let j = 0; j < channels[i].reward.length; j++) {
            console.log("reward_id: ",channels[i].reward[j].reward_id);
            // console.log("broadcaster_id: ",channels[i].broadcaster_id);
            // console.log("token_twitch: ",channels[i].token_twitch);
            await get_redemptions_list(channels[i].reward[j].reward_id,channels[i].broadcaster_id,channels[i].token_twitch);
        }
    }
}

async function get_redemptions_list(reward_id,broadcaster_id,token_twitch) {
    const instance_get_redemptions_list = axios.create({
        baseURL: 'https://api.twitch.tv/',
        headers: {
            'Client-Id': credenciais.client_id_twitch,
            'Authorization': `Bearer ${token_twitch}`,
            'Content-Type': 'application/json'
        }
    });
    try {
        const response = await instance_get_redemptions_list.get(`helix/channel_points/custom_rewards/redemptions?broadcaster_id=${broadcaster_id}&reward_id=${reward_id}&status=UNFULFILLED`);

        let redemptions = response.data.data;
        console.log("quantidade recompensas retiradas: ",redemptions.length);
        for (let i = 0; i < redemptions.length; i++) {
            let quant = parseInt(redemptions[i].reward.cost/15);
            if (broadcaster_id == credenciais.broadcaster_id_duartanath) {
                quant = parseInt(redemptions[i].reward.cost/20);
            }
            let nome = redemptions[i].user_login;
            // let broadcaster_id = redemptions[i].broadcaster_id;
            let reward_perso_id = redemptions[i].reward.id;
            let id = redemptions[i].id;
            // console.log("nome: ",nome);
            // console.log("quant: ",quant);
            // console.log("broadcaster_id: ",broadcaster_id);
            // console.log("reward_perso_id: ",reward_perso_id);
            // console.log("id: ",id);
            try {
                await addpoints(quant,nome);
                await changeStatus('FULFILLED',broadcaster_id,reward_perso_id,id,quant,nome,token_twitch);
            } catch (error) {
                console.log("error try cat",error);
            }
        }
    } catch (error) {
        // console.log("error financeiro: ", error);
        if (error.response) {
        console.log("error get_redemptions_list response: ", error.response.data.message);
        console.log("response get_redemptions_list response: ", error.response);
        } else if (error.request) {
        console.log("error get_redemptions_list request: ", error.message);
        } else {
        console.log("error get_redemptions_list: ", error.message);
        }
    }
}

async function addpoints(quant,nomeUser) {
    try {
        const response = await instance2.put(`kappa/v2/points/${credenciais.id_canal_streamElements_jockerz}/${nomeUser}/${quant}`)
        console.log("response addpoints");
        console.log(response.data);
        return response;
    } catch (error) {
        console.log("error addpoints: ", error);
        if (error.response) {
        console.log("error addpoints: ", error.response.data.message);
        } else if (error.request) {
        console.log("error addpoints: ", error.message);
        } else {
        console.log("error addpoints: ", error.message);
        }
    }
}

async function changeStatus(status,broadcaster_id,reward_id,id,quant,nome,token_twitch) {
    const instance_changeStatus = axios.create({
        baseURL: 'https://api.twitch.tv/',
        headers: {
            'Client-Id': credenciais.client_id_twitch,
            'Authorization': `Bearer ${token_twitch}`,
            'Content-Type': 'application/json'
        }
    });
    const body = {
        status:status
    }
    try {
        const response = await instance_changeStatus.patch(`helix/channel_points/custom_rewards/redemptions?broadcaster_id=${broadcaster_id}&reward_id=${reward_id}&id=${id}`,body)
        
        // handle success
        console.log("response changeStatus");
        console.log(response.data);
        return response;
    } catch (error) {
        addpoints(-(quant),nome);
        console.log("error changeStatus: ", error);
        if (error.response) {
        console.log("error changeStatus: ", error.response.data.message);
        } else if (error.request) {
        console.log("error changeStatus: ", error.message);
        } else {
        console.log("error changeStatus: ", error.message);
        }
    }
}

async function refreshToken(refresh_token) {
    const instance_refreshToken = axios.create({
        baseURL: 'https://id.twitch.tv/'
    });
    try {
        let url = `grant_type=refresh_token&refresh_token=${refresh_token}&client_id=${credenciais.client_id_twitch}&client_secret=${credenciais.client_secret_id_twitch}`;
        // console.log("url: ",url);
        const response = await instance_refreshToken.post(`/oauth2/token?${url}`);
        console.log("response refreshToken.expires_in: ",response.data.expires_in);
        return {access_token:response.data.access_token,expires_in:response.data.expires_in};
    } catch (error) {
        // console.log("error refreshToken: ", error);
        if (error.response) {
        console.log("error refreshToken response: ", error.response.data.message);

        } else if (error.request) {
        console.log("error refreshToken request: ", error.message);
        } else {
        console.log("error refreshToken ultimo: ", error.message);
        }
    }
}

async function changeToken() {
    for (let i = 0; i < channels.length; i++) {
        try {
            console.log("channels["+i+"].token_twitch antes: ",channels[i].token_twitch);
            const new_token = await refreshToken(channels[i].refresh_token_twitch);
            channels[i].token_twitch = new_token.access_token;
            let timeRefresh_temp = (new_token.expires_in - 600) * 1000;
            timeRefresh = timeRefresh_temp < timeRefresh?timeRefresh_temp:timeRefresh;
            // console.log("Token atualizado com sucesso: ",new_token);
            // console.log("New timeRefresh: ",timeRefresh);
            console.log("channels["+i+"].token_twitch depois: ",channels[i].token_twitch);
        } catch (error) {
            console.log("Erro ao atualizar o token: ",error);
        }
        if (i == channels.length-1) {
            console.log("terminou o for: ",i,"  -  ", channels.length-1);
            console.log("timeRefresh: ", timeRefresh);
            setTimeout(() => {
                changeToken();
            }, timeRefresh);
        }
    }
}

setInterval(() => {
    inicio();
}, 30000);

changeToken();